# GIT Workshop
## 13 January 2018
### Project Lab, CS Department, SOE, CUSAT

DAKF CUSAT, in accordance with FOSS Club CUSAT and ICFOSS organized a workshop on **GIT Basics** from 9.30 am to 1 pm. The session was handled by **_Balasankar C_**,*Junior Build Engineer, Gitlab*. 20 students from various departments actively participated in the workshop along with the 4 organizers.
	A telegram group is created to continue the activities of FOSS Club in CUSAT as well as to learn more by doing collaborative projects together.
	The participants were also introduced to FOSS philosophy and RIOT app which is completely free and open source.

Those who want to join CUSAT FOSS club telegram group can use this link 

 https://t.me/fosscusat

And to join Riot group can use the link 

 https://matrix.to/#/%23fossclubcusat:matrix.org 

 or 

 #fossclubcusat:matrix.org

## The details of class took by Balasankar C can be found here: [GIT Tutorial](https://gitlab.com/fosscusat/tutorials/blob/master/git-tutorial.md)

### Some Photos of the programme are posted below.

![Click to see the image](photos/photo_2018-01-15_02-33-23.jpg "GIT Workshop 1")

![Click to see the image](photos/photo_2018-01-15_02-33-45.jpg "GIT Workshop 2")

![Click to see the image](photos/photo_2018-01-15_02-34-21.jpg "GIT Workshop 3")

![Click to see the image](photos/photo_2018-01-15_02-35-25.jpg "GIT Workshop 4")

